#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Test macros
#epicsEnvSet("P", "PC:")

# Macros
epicsEnvSet("P", "$(IOCPREFIX)")

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

motorSimCreateController("motorSim1", 8)
#asynSetTraceIOMask("motorSim1", 0, HEX)
#asynSetTraceMask("motorSim1", 0, ERROR|FLOW|DRIVER)

dbLoadTemplate("${TOP}/iocBoot/${IOC}/motorSim.substitutions", "P=$(P)")

# motorSimConfigAxis(port, axis, lowLimit, highLimit, home, start)
#motorSimConfigAxis("motorSim1", 0, 20000, -20000,  500, 0)
#motorSimConfigAxis("motorSim1", 1, 20000, -20000, 1500, 0)
#motorSimConfigAxis("motorSim1", 2, 20000, -20000, 2500, 0)
#motorSimConfigAxis("motorSim1", 3, 20000, -20000, 3000, 0)
#motorSimConfigAxis("motorSim1", 4, 20000, -20000,  500, 0)
#motorSimConfigAxis("motorSim1", 5, 20000, -20000, 1500, 0)
#motorSimConfigAxis("motorSim1", 6, 20000, -20000, 2500, 0)
#motorSimConfigAxis("motorSim1", 7, 20000, -20000, 3000, 0)


cd "${TOP}/iocBoot/${IOC}"

# autosave settings
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_requestfile_path("$(MOTOR)/db")
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

# autosave
create_monitor_set("auto_settings.req", 30, "P=$(P)")

## Start any sequence programs
#seq sncxxx,"user=epics"
